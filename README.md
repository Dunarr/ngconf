# ngconf
An experimental vhost manager for nginx

## installation
run `sh ngconf-install.sh`

## autocompletion
add: `source /home/USERNAME/.ngconfrc` to your rc file (.bashrc / .zshrc)

run `source .bashrc` or `source .zshrc`

## Usage:
`ngconf create CONF_NAME PROJECT_DIRECTORY PROJECT_URL`
 
`ngconf edit CONF_NAME`
 
`ngconf enable CONF_NAME`

`ngconf disable CONF_NAME`
